#include <string.h>

#include <account.h>
#include <accountopt.h>
#include <cmds.h>
#include <connection.h>
#include <conversation.h>
#include <debug.h>
#include <notify.h>
#include <plugin.h>
#include <pluginpref.h>
#include <prefs.h>
#include <util.h>
#include <wordexp.h>
#include <unistd.h>

#define INI_IMPLEMENTATION
#include "ini/ini.h"

ini_t ini = (ini_t){0};

#define PLUGIN_STATIC_NAME "ircsuppress"
#define PLUGIN_ID "core-rlaager-" PLUGIN_STATIC_NAME
#define PLUGIN_AUTHOR "Alexey Kuznetsov <axet@me.com>"

#define IRC_PLUGIN_ID "prpl-irc"

#define PP_VERSION "0.0.3"
#define PP_WEBSITE "https://gitlab.com/axet/ircsuppress"
#define _(x) x

#define DOMAIN_SUFFIX_DALNET ".dal.net"
#define DOMAIN_SUFFIX_FREENODE ".freenode.net"
#define DOMAIN_SUFFIX_FUNCOM ".funcom.com"
#define DOMAIN_SUFFIX_GAMESURGE ".gamesurge.net"
#define DOMAIN_SUFFIX_INDIEZEN ".indiezen.org"
#define DOMAIN_SUFFIX_JEUX ".jeux.fr"
#define DOMAIN_SUFFIX_QUAKENET ".quakenet.org"
#define DOMAIN_SUFFIX_SPIDERNET ".spidernet.org"
#define DOMAIN_SUFFIX_THUNDERCITY ".thundercity.org"
#define DOMAIN_SUFFIX_UNDERNET ".undernet.org"

#define NICK_CHANSERV "ChanServ"
#define NICK_FREENODE_CONNECT "frigg"
#define NICK_FUNCOM_Q_SERVICE NICK_QUAKENET_Q "@cserve.funcom.com"
#define NICK_GAMESURGE_AUTHSERV "AuthServ"
#define NICK_GAMESURGE_AUTHSERV_SERVICE \
  NICK_GAMESURGE_AUTHSERV "@Services.GameSurge.net"
#define NICK_GAMESURGE_GLOBAL "Global"
#define NICK_MEMOSERV "MemoServ"
#define NICK_NICKSERV "NickServ"
#define NICK_SASLSERV "SaslServ"
#define NICK_DALNET_AUTHSERV_SERVICE NICK_NICKSERV "@services.dal.net"
#define NICK_QUAKENET_Q "Q"
#define NICK_QUAKENET_Q_SERVICE NICK_QUAKENET_Q "@CServe.quakenet.org"
#define NICK_UNDERNET_X "x@channels.undernet.org"

/*****************************************************************************
 * Prototypes                                                                *
 *****************************************************************************/

static gboolean plugin_load(PurplePlugin *plugin);
static gboolean plugin_unload(PurplePlugin *plugin);

/*****************************************************************************
 * Plugin Info                                                               *
 *****************************************************************************/

static PurplePluginInfo info =
{
  PURPLE_PLUGIN_MAGIC,
  2,
  0,
  PURPLE_PLUGIN_STANDARD,          /**< type           */
  NULL,                            /**< ui_requirement */
  0,                               /**< flags          */
  NULL,                            /**< dependencies   */
  PURPLE_PRIORITY_DEFAULT,         /**< priority       */
  PLUGIN_ID,                       /**< id             */
  NULL,                            /**< name           */
  PP_VERSION,                      /**< version        */
  NULL,                            /**< summary        */
  NULL,                            /**< description    */
  PLUGIN_AUTHOR,                   /**< author         */
  PP_WEBSITE,                      /**< homepage       */
  plugin_load,                     /**< load           */
  plugin_unload,                   /**< unload         */
  NULL,                            /**< destroy        */

  NULL,                            /**< ui_info        */
  NULL,                            /**< extra_info     */
  NULL,                            /**< prefs_info     */
  NULL,                            /**< actions        */
  NULL,                            /**< reserved 1     */
  NULL,                            /**< reserved 2     */
  NULL,                            /**< reserved 3     */
  NULL                             /**< reserved 4     */
};

typedef GList *(*actions_func)(PurplePlugin *plugin, gpointer context);

typedef struct _SuppressAccount {
  int filled;
  int last_line;
  char ** lines;
  void * notify;
} SuppressAccount;

actions_func old_actions;
int max_lines = 200;
GHashTable * suppressed = 0;

gchar* g_str_concat_free(gchar* g1, gchar* g2) {
  gchar* g = g_strconcat(g1, g2, NULL);
  g_free(g1);
  return g;
}

void notify_close(SuppressAccount * v) {
  v->notify = 0;
}

static void suppress_show_notify(PurpleConnection *gc) {
  char *title, *body;

  SuppressAccount * v = (SuppressAccount*) g_hash_table_lookup(suppressed, gc->account);
  if (v == 0) {
    v = g_malloc0(sizeof(SuppressAccount));
    g_hash_table_insert(suppressed, gc->account, v);
  }

  const gchar *name = purple_account_get_username(gc->account);
  title = g_strdup_printf(_("Suppressed messages for %s"), name);
  body = g_strdup("<span style=\"font-family: monospace;\">");
  if ( v->lines != 0 ) {
    if ( v->filled ) {
      int i = v->last_line;
      do {
        body = g_str_concat_free(body, v->lines[i]);
        body = g_str_concat_free(body, "<br/>");
        i = (i + 1) % max_lines;
      } while(i != v->last_line);
    } else {
      int i = 0;
      while (i != v->last_line) {
        body = g_str_concat_free(body, v->lines[i]);
        body = g_str_concat_free(body, "<br/>");
        i = (i + 1) % max_lines;
      }
    }
  } else {
    body = g_str_concat_free(body, "No Suppressed messages");
  }
  body = g_str_concat_free(body, "</span>");
  if ( v->notify != 0 )
    purple_notify_close(PURPLE_NOTIFY_FORMATTED, v->notify);
  v->notify = purple_notify_formatted(gc, title, title, NULL, body, (PurpleNotifyCloseCallback)notify_close, v);
  g_free(title);
  g_free(body);
}

static void irc_view_motd(PurplePluginAction *action)
{
  PurpleConnection *gc = (PurpleConnection *) action->context;

  if (gc == NULL || gc->proto_data == NULL) {
    purple_debug(PURPLE_DEBUG_ERROR, "irc", "got MOTD request for NULL gc\n");
    return;
  }

  suppress_show_notify(gc);
}

static void suppress_message(PurpleAccount* account, gchar* nick, gchar* msg) {
  SuppressAccount * v = g_hash_table_lookup(suppressed, account);
  if (v == 0) {
    v = g_malloc0(sizeof(SuppressAccount));
    g_hash_table_insert(suppressed, account, v);
  }
  
  if ( v->lines == 0 ) {
    v->lines = malloc(sizeof(char*) * max_lines);
    for (int i = 0; i < max_lines; i++)
      v->lines[i] = g_strdup("");
  }
  GDateTime *date = g_date_time_new_now_local();
  gchar *str = g_date_time_format(date, "%F %H:%M:%S");
  gchar *line = g_strdup_printf("(%s) %s: %s", str, nick, msg);
  g_free(v->lines[v->last_line]);
  v->lines[v->last_line++] = line;
  v->last_line = v->last_line % max_lines;
  if (v->last_line == 0)
    v->filled = TRUE;
  g_free(str);
  g_date_time_unref(date);

  if (v->notify != 0)
    suppress_show_notify(account->gc);
}

gboolean ini_test(const char* var, const char* nick, gboolean (*fn)(const char*, const char*)) {
  if (ini_is_valid(&ini)) {
    initable_t *root = ini_get_table(&ini, INI_ROOT);
    inivec_t(inistrv_t) arr = ini_as_array(ini_get(root, var), ' ');
    for (inistrv_t *v = arr; v != ivec_end(arr); ++v) {
      char buf[1024];
      snprintf(buf, sizeof(buf), "%.*s", (int)v->len, v->buf);
      if (fn(nick, buf)) {
        ivec_free(arr);
        return TRUE;
      }
    }
    ivec_free(arr);
  }
  return FALSE;
}

gboolean suppress_nick(gchar * nick) {
  gchar * NN[] = { NICK_CHANSERV, NICK_NICKSERV, NICK_SASLSERV, "CTCPServ", "coulomb.oftc.net", "ssl.irc.atw-inter.net", NICK_FREENODE_CONNECT,
    NICK_FUNCOM_Q_SERVICE, NICK_GAMESURGE_AUTHSERV, NICK_GAMESURGE_AUTHSERV_SERVICE, NICK_GAMESURGE_GLOBAL, NICK_MEMOSERV,
    NICK_DALNET_AUTHSERV_SERVICE, NICK_QUAKENET_Q_SERVICE, NICK_UNDERNET_X, "liquid.oftc.net"};
  for (int i = 0; i < sizeof(NN)/sizeof(NN[0]); i++) {
    if (g_str_equal(nick, NN[i]))
      return TRUE;
  }
  if (ini_test("nick", nick, (gboolean (*)(const char*, const char*)) g_str_equal))
    return TRUE;
  gchar * SS[] = { ".oftc.net", ".atw-inter.net", DOMAIN_SUFFIX_DALNET, DOMAIN_SUFFIX_FREENODE,
    DOMAIN_SUFFIX_FUNCOM, DOMAIN_SUFFIX_GAMESURGE, DOMAIN_SUFFIX_INDIEZEN, DOMAIN_SUFFIX_JEUX,
    DOMAIN_SUFFIX_QUAKENET, DOMAIN_SUFFIX_SPIDERNET, DOMAIN_SUFFIX_THUNDERCITY, DOMAIN_SUFFIX_UNDERNET
  };
  for (int i = 0; i < sizeof(SS)/sizeof(SS[0]); i++) {
    if (g_str_has_suffix(nick, SS[i]))
      return TRUE;
  }
  if (ini_test("suffix", nick, g_str_has_suffix))
     return TRUE;
  return FALSE;
}

static gboolean receiving_im_msg_cb(PurpleAccount *account, gchar **sender,
                                    gchar **buffer,
                                    PurpleConversation *conv,
                                    gint *flags, gpointer data)
{
  gchar *msg;
  gchar *nick;
  PurpleConnection *connection;

  if (!g_str_equal(purple_account_get_protocol_id(account), IRC_PLUGIN_ID))
    return FALSE;
  
  if (conv != 0)
    return FALSE;

  msg = *buffer;
  nick = *sender;

  connection = purple_account_get_connection(account);
  g_return_val_if_fail(NULL != connection, FALSE);

  if (suppress_nick(nick)) {
    if (conv == 0) {
      PurpleLog * log = purple_log_new(PURPLE_LOG_IM, nick, account, conv, time(NULL), NULL);
      purple_log_write(log, *flags, nick, time(NULL), msg);
      purple_log_free(log);
    } else {
      purple_conversation_write(conv, nick, msg, *flags, time(NULL));
    }
    suppress_message(account, nick, msg);
    return TRUE;
  }

  return FALSE;
}

GList * actions(PurplePlugin *plugin, gpointer context) {
  GList * list = old_actions(plugin, context);

  PurplePluginAction *act = NULL;

  act = purple_plugin_action_new(_("Suppressed"), irc_view_motd);
  list = g_list_append(list, act);

  return list;
}

static gboolean plugin_load(PurplePlugin *plugin)
{
  PurplePlugin *irc_prpl;
  PurplePluginProtocolInfo *prpl_info;
  void *conv_handle;

  irc_prpl = purple_plugins_find_with_id(IRC_PLUGIN_ID);

  if (NULL == irc_prpl)
    return FALSE;

  prpl_info = PURPLE_PLUGIN_PROTOCOL_INFO(irc_prpl);
  if (NULL == prpl_info)
    return FALSE;

  old_actions = irc_prpl->info->actions;
  irc_prpl->info->actions = actions;

  suppressed = g_hash_table_new_full(0, 0, 0, g_free);

  conv_handle = purple_conversations_get_handle();
  purple_signal_connect(conv_handle, "receiving-im-msg",
                        plugin, PURPLE_CALLBACK(receiving_im_msg_cb),
                        NULL);

  wordexp_t p;
  wordexp( "$HOME/.config/ircsuppress.ini", &p, 0 );
  if (access(p.we_wordv[0], F_OK) == 0)
    ini = ini_parse(p.we_wordv[0], NULL);
  wordfree( &p );

  return TRUE;
}

static gboolean plugin_unload(PurplePlugin *plugin)
{
  PurplePlugin *irc_prpl;
  PurplePluginProtocolInfo *prpl_info;
  GList *list;

  irc_prpl = purple_plugins_find_with_id(IRC_PLUGIN_ID);
  if (NULL == irc_prpl)
    return FALSE;

  prpl_info = PURPLE_PLUGIN_PROTOCOL_INFO(irc_prpl);
  if (NULL == prpl_info)
    return FALSE;

  list = prpl_info->protocol_options;

  /* Remove protocol preferences. */
  while (NULL != list)
  {
    PurpleAccountOption *option = (PurpleAccountOption *) list->data;

    if (g_str_has_prefix(purple_account_option_get_setting(option), PLUGIN_ID "_"))
    {
      GList *llist = list;

      /* Remove this element from the list. */
      if (llist->prev)
        llist->prev->next = llist->next;
      if (llist->next)
        llist->next->prev = llist->prev;

      purple_account_option_destroy(option);

      list = g_list_next(list);

      g_list_free_1(llist);
    }
    else
      list = g_list_next(list);
  }

  g_hash_table_destroy(suppressed);
  suppressed = 0;

  return TRUE;
}

static void plugin_init(PurplePlugin *plugin)
{
  info.dependencies = g_list_append(info.dependencies, IRC_PLUGIN_ID);

#ifdef ENABLE_NLS
  bindtextdomain(GETTEXT_PACKAGE, PP_LOCALEDIR);
  bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
#endif /* ENABLE_NLS */

  info.name = _("IRC Suppress");
  info.summary = _("Handles the rough edges of the IRC protocol.");
  info.description = _("- Suppression of various useless messages");
}

PURPLE_INIT_PLUGIN(PLUGIN_STATIC_NAME, plugin_init, info)
