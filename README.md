# Pidgin IRC Suppress Plugin

This plugin was developed to suppress automatic messages from IRC.

## USAGE

Once you have the plugin installed, activate it (Tools -> Plugins).

The plugin suppresses lots of useless messages with no configuration.

All suppressed messages appears in:

* Accounts / [ IRC Account ] / Suppressed
* Conversation history.
* /query NickServ

# Config

Config file located at `$HOME/.config/ircsuppress.ini` with following format:

```
nick = supressnick1 supressnick2
suffix = tngnet.ircnet.io test.test.test
```
